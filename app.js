// Arrow function

// const logger = (log) => {
//     console.log(log);
// }
// const logger = log => console.log(log);
// console.log('Message...');

// const sum = (a, b) => {
//     return a + b;
// }

// const sum = (a, b) => a + b;

// const sum = (a, b) => {
//     return {
//         a: a,
//         b: b,
//     }
// }

// const sum = (a, b) => ({
//     a: a,
//     b: b
// })
// console.log(sum(1, 2));

// const course = {
//     name: 'Javascript basic!',
//     getName: function () {
//         return this; //context
//     }
// }
// console.log(course.getName());

// const Course = function (name, price) {
//     this.name = name;
//     this.price = price;
// }

// const jsCourse = new Course('Javascript', 1000);

// console.log(jsCourse);

// var name = 'Javascript';
// var price = 1000;
// var course = {
//     name,
//     price,
//     getName () {
//         return name;
//     }
// }

// console.log(course.getName());

// var fieldName = 'name';
// var fieldPrice = 'price';

// const course = {
//     [fieldName]: 'Javascript',
//     [fieldPrice]: 1000
// }

// console.log(course);

// const obj1 = arrToObj([
//     ['name', 'Son Dang'],
//     ['age', 21],
//     ['address', 'Ha Noi']
// ])

// function arrToObj(arr) {
//     return Object.fromEntries(arr)
// }

// console.log(obj1)

// const arr = ['zero', 'one', 'two'];

// const obj = arr.reduce((accumulator, value, index) => {
//     return { ...accumulator, ['key' + index]: value };
// }, {});

// // 👇️️ {'key0': 'zero', 'key1': 'one', 'key2': 'two'}
// console.log(obj);


// var array = ['Javascript', 'PHP', 'Ruby'];

// var [a, ...rest] = array;

// console.log(a);
// console.log(rest);

// var course = {
//     name: 'Javascript',
//     price: 1000,
//     image: 'image-address',
// }
// var { name, description = 'default description' } = course;

// console.log(name);
// console.log(description);

// function logger(...params) {
//     console.log(params);
// }

// console.log(logger(1, 2, 3, 4, 5, 6, 7, 8));


// function logger(name, price, ...rest) {
//     console.lof(name);
//     console.log(price);
//     console.log(...rest);
// }

// console.log({
//     name: ' Javascript',
//     price: 1000,
//     description: 'Description content'
// })

// function logger([a, b, ...rest]) {
//     console.log(a);
//     console.log(b);
//     console.log(rest)
// }

// logger([2, 6, 1, 2, 3, 4, 5]);

// var array1 = ['Javascript', 'Ruby', 'PHP'];
// var array2 = ['ReactJS', 'Dart'];
// var array3 = [];

// // var array3 = array1.concat(array2);
// array3 = [...array2, ...array1];
// console.log(array3);

// var object1 = {
//     name: 'Javascript'
// };
// var object2 = {
//     price: 1000
// };

// var object3 = {
//     ...object1,
//     ...object2
// }

// console.log(object3)

import { logger2 } from './logger/index.js';
// import { TYPE_LOG, TYPE_WARN, TYPE_ERROR } from './const.js';
import * as constants from './const.js'
logger2('Test message....', constants.TYPE_WARN);
console.log(constants);